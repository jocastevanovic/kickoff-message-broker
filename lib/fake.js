"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.init = exports.sendRPCMessage = exports.consumeRPCQueue = exports.sendMessage = exports.consumeQueue = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _debuggers = require("./debuggers");

var server;
var envs;

var consumeQueue = function consumeQueue(queueName) {
  (0, _debuggers.ddInfo)("Adding post for ".concat(queueName));
};

exports.consumeQueue = consumeQueue;

var sendMessage = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(queueName) {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            (0, _debuggers.ddInfo)("Sent message to ".concat(queueName));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function sendMessage(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.sendMessage = sendMessage;
var consumeRPCQueue = consumeQueue;
exports.consumeRPCQueue = consumeRPCQueue;

var sendRPCMessage = function sendRPCMessage(queueName) {
  return sendMessage(queueName);
};

exports.sendRPCMessage = sendRPCMessage;

var init = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(options) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            server = options.server;
            envs = options.envs;

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function init(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.init = init;