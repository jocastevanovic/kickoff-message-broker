"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

exports.__esModule = true;
exports.init = exports.sendMessage = exports.sendRPCMessage = exports.consumeRPCQueue = exports.consumeQueue = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var amqplib = _interopRequireWildcard(require("amqplib"));

var _uuid = require("uuid");

var _debuggers = require("./debuggers");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var channel;
var queuePrefix;

var consumeQueue = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(queueName, callback) {
    var args,
        _args2 = arguments;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            args = _args2.length > 2 && _args2[2] !== undefined ? _args2[2] : {};
            queueName = queueName.concat(queuePrefix);
            _context2.next = 4;
            return channel.assertQueue(queueName, _objectSpread({
              durable: true
            }, args));

          case 4:
            _context2.next = 6;
            return channel.prefetch(1);

          case 6:
            try {
              channel.consume(queueName, /*#__PURE__*/function () {
                var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(msg) {
                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!(msg !== null)) {
                            _context.next = 10;
                            break;
                          }

                          _context.prev = 1;
                          _context.next = 4;
                          return callback(JSON.parse(msg.content.toString()), msg);

                        case 4:
                          _context.next = 9;
                          break;

                        case 6:
                          _context.prev = 6;
                          _context.t0 = _context["catch"](1);
                          (0, _debuggers.ddError)("Error occurred during callback ".concat(queueName, " ").concat(_context.t0.message));

                        case 9:
                          channel.ack(msg);

                        case 10:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, null, [[1, 6]]);
                }));

                return function (_x3) {
                  return _ref2.apply(this, arguments);
                };
              }(), {
                noAck: false
              });
            } catch (e) {
              (0, _debuggers.ddError)("Error occurred during consumeq queue ".concat(queueName, " ").concat(e.message));
            }

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function consumeQueue(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.consumeQueue = consumeQueue;

var consumeRPCQueue = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(queueName, callback) {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            queueName = queueName.concat(queuePrefix);
            _context4.prev = 1;
            _context4.next = 4;
            return channel.assertQueue(queueName, {
              durable: true
            });

          case 4:
            _context4.next = 6;
            return channel.prefetch(1);

          case 6:
            channel.consume(queueName, /*#__PURE__*/function () {
              var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(msg) {
                var response;
                return _regenerator["default"].wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (!(msg !== null)) {
                          _context3.next = 13;
                          break;
                        }

                        (0, _debuggers.ddInfo)("Received rpc queue message ".concat(msg.content.toString(), " from queue ").concat(queueName, " !"));
                        _context3.prev = 2;
                        _context3.next = 5;
                        return callback(JSON.parse(msg.content.toString()));

                      case 5:
                        response = _context3.sent;
                        channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(response)), {
                          correlationId: msg.properties.correlationId,
                          persistent: true
                        });
                        _context3.next = 12;
                        break;

                      case 9:
                        _context3.prev = 9;
                        _context3.t0 = _context3["catch"](2);
                        (0, _debuggers.ddError)("Error occurred during callback ".concat(queueName, " ").concat(_context3.t0.message));

                      case 12:
                        channel.ack(msg);

                      case 13:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, null, [[2, 9]]);
              }));

              return function (_x6) {
                return _ref4.apply(this, arguments);
              };
            }(), {
              noAck: false
            });
            _context4.next = 12;
            break;

          case 9:
            _context4.prev = 9;
            _context4.t0 = _context4["catch"](1);
            (0, _debuggers.ddError)("Error occurred during consume rpc queue ".concat(queueName, " ").concat(_context4.t0.message));

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[1, 9]]);
  }));

  return function consumeRPCQueue(_x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.consumeRPCQueue = consumeRPCQueue;

var sendRPCMessage = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(queueName, message) {
    var response;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            queueName = queueName.concat(queuePrefix);
            (0, _debuggers.ddInfo)("Sending rpc message ".concat(JSON.stringify(message), " to queue ").concat(queueName, " !"));
            _context5.next = 4;
            return new Promise(function (resolve, reject) {
              var correlationId = (0, _uuid.v4)();
              return channel.assertQueue("", {
                exclusive: true,
                durable: true
              }).then(function (q) {
                channel.consume(q.queue, function (msg) {
                  if (!msg) {
                    return reject(new Error("consumer cancelled by rabbitmq"));
                  }

                  if (msg.properties.correlationId === correlationId) {
                    var res = JSON.parse(msg.content.toString());

                    if (res && res.status === "success") {
                      resolve(res.data);
                    } else if (res) {
                      reject(new Error(res.errorMessage));
                    } else {
                      reject(new Error("msg.content is null!"));
                    }

                    channel.deleteQueue(q.queue);
                  }
                }, {
                  noAck: true
                });
                channel.sendToQueue(queueName, Buffer.from(JSON.stringify(message)), {
                  correlationId: correlationId,
                  replyTo: q.queue,
                  persistent: true
                });
              });
            });

          case 4:
            response = _context5.sent;
            return _context5.abrupt("return", response);

          case 6:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function sendRPCMessage(_x7, _x8) {
    return _ref5.apply(this, arguments);
  };
}();

exports.sendRPCMessage = sendRPCMessage;

var sendMessage = /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(queueName, data, args) {
    var message;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            queueName = queueName.concat(queuePrefix);
            if (!args) args = {};
            _context6.prev = 2;
            message = JSON.stringify(data || {});
            (0, _debuggers.ddInfo)("Sending message ".concat(message, " to ").concat(queueName, " !"));
            _context6.next = 7;
            return channel.assertQueue(queueName, _objectSpread({
              durable: true
            }, args));

          case 7:
            _context6.next = 9;
            return channel.sendToQueue(queueName, Buffer.from(message), {
              persistent: true
            });

          case 9:
            _context6.next = 14;
            break;

          case 11:
            _context6.prev = 11;
            _context6.t0 = _context6["catch"](2);
            (0, _debuggers.ddError)("Error occurred during send queue ".concat(queueName, " ").concat(_context6.t0.message));

          case 14:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[2, 11]]);
  }));

  return function sendMessage(_x9, _x10, _x11) {
    return _ref6.apply(this, arguments);
  };
}();

exports.sendMessage = sendMessage;

var init = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(RABBITMQ_HOST, prefix) {
    var connection;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return amqplib.connect(RABBITMQ_HOST);

          case 2:
            connection = _context7.sent;
            _context7.next = 5;
            return connection.createChannel();

          case 5:
            channel = _context7.sent;
            queuePrefix = prefix;

          case 7:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function init(_x12, _x13) {
    return _ref7.apply(this, arguments);
  };
}();

exports.init = init;