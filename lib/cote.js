"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

exports.__esModule = true;
exports.init = exports.sendMessage = exports.consumeRPCQueue = exports.sendRPCMessage = exports.consumeQueue = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var cote = _interopRequireWildcard(require("cote"));

var _debuggers = require("./debuggers");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var publisher;
var subscriber;
var name;
var requesters = {};
var responders = {};

var consumeQueue = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(queueName, callback) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!subscriber) {
              subscriber = new cote.Subscriber({
                name: "".concat(name, "Subscriber")
              });
            }

            subscriber.on(queueName, /*#__PURE__*/function () {
              var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req) {
                return _regenerator["default"].wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.prev = 0;
                        _context.next = 3;
                        return callback(JSON.parse(req));

                      case 3:
                        _context.next = 8;
                        break;

                      case 5:
                        _context.prev = 5;
                        _context.t0 = _context["catch"](0);
                        (0, _debuggers.ddError)("Error occurred during callback ".concat(queueName, " ").concat(_context.t0.message));

                      case 8:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, null, [[0, 5]]);
              }));

              return function (_x3) {
                return _ref2.apply(this, arguments);
              };
            }());

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function consumeQueue(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.consumeQueue = consumeQueue;

var sendRPCMessage = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(queueName, message) {
    var response;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (!requesters[queueName]) {
              requesters[queueName] = new cote.Requester({
                name: "".concat(queueName, " requester"),
                key: queueName
              });
            }

            (0, _debuggers.ddInfo)("Sending rpc message ".concat(JSON.stringify(message), " to queue ").concat(queueName));
            _context3.next = 4;
            return requesters[queueName].send({
              type: queueName,
              message: JSON.stringify(message)
            });

          case 4:
            response = _context3.sent;

            if (!(response.status === "success")) {
              _context3.next = 9;
              break;
            }

            return _context3.abrupt("return", response.data);

          case 9:
            throw new Error(response.errorMessage);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function sendRPCMessage(_x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.sendRPCMessage = sendRPCMessage;

var consumeRPCQueue = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(queueName, callback) {
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            if (!responders[queueName]) {
              responders[queueName] = new cote.Responder({
                name: "".concat(queueName, " responder"),
                key: queueName
              });
            }

            responders[queueName].on(queueName, /*#__PURE__*/function () {
              var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, cb) {
                var response;
                return _regenerator["default"].wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        _context4.prev = 0;
                        _context4.next = 3;
                        return callback(JSON.parse(req.message));

                      case 3:
                        response = _context4.sent;
                        cb(null, response);
                        _context4.next = 10;
                        break;

                      case 7:
                        _context4.prev = 7;
                        _context4.t0 = _context4["catch"](0);
                        (0, _debuggers.ddError)("Error occurred during callback ".concat(queueName, " ").concat(_context4.t0.message));

                      case 10:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4, null, [[0, 7]]);
              }));

              return function (_x8, _x9) {
                return _ref5.apply(this, arguments);
              };
            }());

          case 2:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function consumeRPCQueue(_x6, _x7) {
    return _ref4.apply(this, arguments);
  };
}();

exports.consumeRPCQueue = consumeRPCQueue;

var sendMessage = /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(queueName, data) {
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            (0, _debuggers.ddInfo)("Sending message to ".concat(queueName));

            if (!publisher) {
              publisher = new cote.Publisher({
                name: "".concat(name, "Publisher")
              });
            }

            publisher.publish(queueName, Buffer.from(JSON.stringify(data || {})));

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function sendMessage(_x10, _x11) {
    return _ref6.apply(this, arguments);
  };
}();

exports.sendMessage = sendMessage;

var init = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(options) {
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            name = options.name;

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function init(_x12) {
    return _ref7.apply(this, arguments);
  };
}();

exports.init = init;