"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

exports.__esModule = true;
exports.init = exports.sendRPCMessage = exports.consumeRPCQueue = exports.sendMessage = exports.consumeQueue = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var requestify = _interopRequireWildcard(require("requestify"));

var _debuggers = require("./debuggers");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var server;
var envs;

var sendRequest = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
    var url,
        method,
        body,
        wait,
        reqParams,
        response,
        responseBody,
        message,
        _args = arguments;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = _ref.url, method = _ref.method, body = _ref.body;
            wait = _args.length > 1 && _args[1] !== undefined ? _args[1] : false;
            (0, _debuggers.ddInfo)("\n    Sending request to\n    url: ".concat(url, "\n    method: ").concat(method, "\n    body: ").concat(JSON.stringify(body), "\n  "));
            reqParams = {
              method: method,
              headers: {
                "Content-Type": "application/json"
              },
              body: body
            };

            if (!wait) {
              _context.next = 24;
              break;
            }

            _context.prev = 5;
            _context.next = 8;
            return requestify.request(url, reqParams);

          case 8:
            response = _context.sent;
            responseBody = response.getBody();
            (0, _debuggers.ddInfo)("\n        Success from : ".concat(url, "\n        responseBody: ").concat(JSON.stringify(responseBody), "\n      "));
            return _context.abrupt("return", responseBody);

          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](5);

            if (!(_context.t0.code === "ECONNREFUSED" || _context.t0.code === "ENOTFOUND")) {
              _context.next = 20;
              break;
            }

            throw new Error(_context.t0.message);

          case 20:
            message = _context.t0.body || _context.t0.message;
            throw new Error(message);

          case 22:
            _context.next = 25;
            break;

          case 24:
            requestify.request(url, reqParams).then(function () {
              return (0, _debuggers.ddInfo)("success");
            })["catch"](function (e) {
              return (0, _debuggers.ddError)(e.message);
            });

          case 25:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[5, 14]]);
  }));

  return function sendRequest(_x) {
    return _ref2.apply(this, arguments);
  };
}();

var getUrl = function getUrl(queueName) {
  var _envs = envs,
      MAIN_APP_DOMAIN = _envs.MAIN_APP_DOMAIN;

  if (queueName === "putLog") {
    if (envs.LOGS_API_DOMAIN) {
      return envs.LOGS_API_DOMAIN;
    }

    return "http://127.0.0.1:3800";
  }

  if (queueName === "kickoffcrm-api:integrations-notification" || queueName === "rpc_queue:api_to_integrations") {
    if (envs.INTEGRATIONS_API_DOMAIN) {
      return envs.INTEGRATIONS_API_DOMAIN;
    }

    return "".concat(MAIN_APP_DOMAIN, "/integrations");
  }

  if (queueName === "engagesNotification") {
    return envs.MAIN_API_DOMAIN;
  }

  if (queueName === "rpc_queue:integrations_to_api") {
    return envs.MAIN_API_DOMAIN;
  }

  if (queueName === "kickoffcrm-api:engages-notification") {
    if (envs.ENGAGES_API_DOMAIN) {
      return envs.ENGAGES_API_DOMAIN;
    }

    return "http://127.0.0.1:3900";
  }

  if (queueName === "rpc_queue:api_to_workers") {
    if (envs.WORKERS_API_DOMAIN) {
      return envs.WORKERS_API_DOMAIN;
    }

    return "http://127.0.0.1:3700";
  }
};

var consumeQueue = function consumeQueue(queueName, callback) {
  (0, _debuggers.ddInfo)("Adding post for ".concat(queueName));
  server.post("/".concat(queueName), /*#__PURE__*/function () {
    var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
      var response, errorMessage;
      return _regenerator["default"].wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              (0, _debuggers.ddInfo)("Received data in ".concat(queueName));
              _context2.prev = 1;
              _context2.next = 4;
              return callback(req.body);

            case 4:
              response = _context2.sent;
              _context2.next = 12;
              break;

            case 7:
              _context2.prev = 7;
              _context2.t0 = _context2["catch"](1);
              errorMessage = "Error occurred during callback ".concat(queueName, " ").concat(_context2.t0.message);
              (0, _debuggers.ddError)(errorMessage);
              return _context2.abrupt("return", res.status(500).send(errorMessage));

            case 12:
              if (response) {
                _context2.next = 14;
                break;
              }

              return _context2.abrupt("return", res.send("ok"));

            case 14:
              if (!(response.status === "success")) {
                _context2.next = 18;
                break;
              }

              return _context2.abrupt("return", res.json(response.data));

            case 18:
              return _context2.abrupt("return", res.status(500).send(response.errorMessage));

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[1, 7]]);
    }));

    return function (_x2, _x3) {
      return _ref3.apply(this, arguments);
    };
  }());
};

exports.consumeQueue = consumeQueue;

var sendMessage = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(queueName, data) {
    var wait,
        response,
        _args3 = arguments;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            wait = _args3.length > 2 && _args3[2] !== undefined ? _args3[2] : false;
            _context3.next = 3;
            return sendRequest({
              url: "".concat(getUrl(queueName), "/").concat(queueName),
              method: "POST",
              body: data
            }, wait);

          case 3:
            response = _context3.sent;
            return _context3.abrupt("return", response);

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function sendMessage(_x4, _x5) {
    return _ref4.apply(this, arguments);
  };
}();

exports.sendMessage = sendMessage;
var consumeRPCQueue = consumeQueue;
exports.consumeRPCQueue = consumeRPCQueue;

var sendRPCMessage = function sendRPCMessage(queueName, data) {
  return sendMessage(queueName, data, true);
};

exports.sendRPCMessage = sendRPCMessage;

var init = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(options) {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            server = options.server;
            envs = options.envs;

          case 2:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function init(_x6) {
    return _ref5.apply(this, arguments);
  };
}();

exports.init = init;