"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

exports.__esModule = true;
exports.ddError = exports.ddInfo = exports.ddLogger = void 0;

var _ddTrace = _interopRequireDefault(require("dd-trace"));

var formats = _interopRequireWildcard(require("dd-trace/ext/formats"));

var _debug = _interopRequireDefault(require("debug"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var ddLogger = function ddLogger(message, level) {
  if (!process.env.DD_SERVICE) {
    console.log(message);
    return (0, _debug["default"])("kickoffcrm-message-broker:")(message);
  }

  var span = _ddTrace["default"].scope().active();

  var time = new Date().toISOString();
  var record = {
    time: time,
    level: level,
    message: message
  };

  if (span) {
    _ddTrace["default"].inject(span.context(), formats.LOG, record);
  }

  console.log(JSON.stringify(record));
};

exports.ddLogger = ddLogger;

var ddInfo = function ddInfo(message) {
  return ddLogger(message, "info");
};

exports.ddInfo = ddInfo;

var ddError = function ddError(message) {
  return ddLogger(message, "error");
};

exports.ddError = ddError;